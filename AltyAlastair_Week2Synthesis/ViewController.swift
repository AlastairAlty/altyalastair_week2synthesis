//
//  ViewController.swift
//  AltyAlastair_Week2Synthesis
//
//  Created by Alastair Alty on 11/6/20.
//

import UIKit

class ViewController: UIViewController {
   
    
    @IBOutlet weak var totalPopulationLabel: UILabel!
    // create dictionary with that holds a dictionary as a value. populate the keys as the continent names. values of country names and population.
    var placeAndPopulation: [String: [String: Int]] = ["Africa": [:], "Asia": [:], "Australia": [:], "Europe": [:], "North America": [:], "South America": [:]]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        totalPopulationLabel.text = "There are \(placeAndPopulation.count) in the world with a population of \(placeAndPopulation)"
        
        // Do any additional setup after loading the view.
        /*totalPopulationLabel.text = "There are \(placeAndPopulation.keys.count) countries in the world with a population of \(placeAndPopulation.values.count)."
 */
    }
    @IBOutlet weak var africaBtn: UIButton!
    @IBOutlet weak var asiaBtn: UIButton!
    
    
    
    
    @IBAction func ViewDetails(_ sender: AnyObject){
        self.performSegue(withIdentifier: "View", sender: self)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "View" {
            let destination = segue.destination as! CountryInfo
            destination.continentString = "Africa"
            //destination.infoOfCountry.text = "\(destination.continentName.text) has \(placeAndPopulation.values.count) with a total population of \(placeAndPopulation.values.count)"

            
            }
            
        }
    }




