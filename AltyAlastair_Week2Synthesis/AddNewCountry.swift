//
//  AddNewCountry.swift
//  AltyAlastair_Week2Synthesis
//
//  Created by Alastair Alty on 11/6/20.
//

import UIKit

class AddNewCountry: UIViewController {
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var newCountryTextField: UITextField!
    @IBOutlet weak var newPopulationLabel: UILabel!
    @IBOutlet weak var newPopulationTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
